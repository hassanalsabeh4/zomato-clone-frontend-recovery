import "bootstrap/dist/css/bootstrap.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ShowRestaurants from "./pages/restaurants/showrestaurants";
// import SideBar from "./pages/sidebar/sidebar";
import Dashboard from "./pages/dashboard/dashboard";
import Login from "./pages/login/Login";
import Categories from "./pages/Categories/Categories";
import Drivers from "./pages/Drivers/Drivers";
import Orders from "./pages/Orders/orders";
import Items from "./pages/Items/Items";
import Register from "./pages/register/register";
import EditRestaurants from "./pages/restaurants/editRestaurants";
import EditCategory from "./pages/Categories/editCategory";
import ProtectedRoute from "./pages/login/ProtectedRoute";
import AddDriver from "./pages/Drivers/AddDriver";
import EditDriver from "./pages/Drivers/editDriver";
import EditOrder from "./pages/Orders/editOrder";
import ViewOrderItem from "./pages/Orders/ViewOrderItem";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/dashboard" exact element={<Dashboard />} />
        <Route path="/view-order/:id" exact element={<ViewOrderItem />} />
        <Route
          path="/show-restaurants"
          exact
          element={
            <ProtectedRoute>
              <ShowRestaurants />
            </ProtectedRoute>
          }
        />
        <Route
          path="/edit-restaurants/:id"
          exact
          element={
            <ProtectedRoute>
              <EditRestaurants />
            </ProtectedRoute>
          }
        />
        <Route
          path="/categories"
          exact
          element={
            <ProtectedRoute>
              <Categories />
            </ProtectedRoute>
          }
        />
        <Route
          path="/edit-category/:id"
          exact
          element={
            <ProtectedRoute>
              <EditCategory />
            </ProtectedRoute>
          }
        />
        <Route
          path="/drivers"
          exact
          element={
            <ProtectedRoute>
              <Drivers />
            </ProtectedRoute>
          }
        />
        <Route
          path="/orders"
          exact
          element={
            <ProtectedRoute>
              <Orders />
            </ProtectedRoute>
          }
        />
        <Route
          path="/items"
          exact
          element={
            <ProtectedRoute>
              <Items />
            </ProtectedRoute>
          }
        />
        <Route path="/" exact element={<Login />} />
        <Route path="/register" exact element={<Register />} />
        <Route path="/add-driver" exact element={<AddDriver />} />
        <Route path="/edit-driver/:id" exact element={<EditDriver />} />
        <Route path="/edit-order/:id" exact element={<EditOrder />} />
      </Routes>
    </Router>
  );
}
export default App;
