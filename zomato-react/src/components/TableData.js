import React from "react";
import Button from "react-bootstrap/Button";
import { useDispatch } from "react-redux";
import { URL } from "../apis/config";
import { deleteitem } from "../redux/actions/itemActions";
function TableData({ item }) {
  const dispatch = useDispatch();
  return (
    <tbody>
      <tr>
        <th scope="row">
          {" "}
          <img
            src={URL + item.photo}
            style={{ maxWidth: "7rem", maxHeight: "4rem" }}
          />
        </th>
        <td>{item.name}</td>
        <td>{item.description}</td>
        <td>{item.price + " LBP"}</td>
        <td>
          <Button
            size="sm"
            variant="danger"
            onClick={() => dispatch(deleteitem(item.id))}
          >
            Delete
          </Button>
        </td>
        <td>
          <Button size="sm" variant="info" style={{ marginRight: "10px" }}>
            Edit
          </Button>
        </td>
        <td>
          <Button size="sm" variant="info" style={{ marginRight: "10px" }}>
            View
          </Button>
        </td>
      </tr>
    </tbody>
  );
}

export default TableData;
