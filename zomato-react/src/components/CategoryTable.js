import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { deleteitem } from "../redux/actions/itemActions";
import { URL } from "../apis/config";
import { updateitem } from "../redux/actions/itemActions";
function CategoryTable({ openModel, category, openUpdateModel }) {
  const dispatch = useDispatch();
  const [inputs, setInputs] = useState({});
  const inputHandler = (e) => {
    setInputs({
      ...inputs,
      [e.target.name]: e.target.value,
    });
    console.log(inputs);
  };
  return (
    <>
      <div class="content-body">
        <section class="form-control-repeater">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">{category.name}</h4>
                </div>
                <div class="card-body">
                  <form action="#" class="invoice-repeater">
                    {category.items.map((item) => (
                      <div data-repeater-list="invoice">
                        <div data-repeater-item="">
                          <div class="row d-flex align-items-end">
                            <div class="col-md-2 col-12">
                              <div class="form-group">
                                <label for="itemname">Image</label>
                                <img
                                  class="form-control"
                                  id="itemname"
                                  style={{
                                    maxWidth: "7rem",
                                    maxHeight: "4rem",
                                  }}
                                  src={URL + item.photo}
                                />
                              </div>
                            </div>
                            <div class="col-md-1 col-12">
                              <div class="form-group">
                                <label for="itemcost">ID</label>
                                <p class="form-control-plaintext" id="itemcost">
                                  {item.id}
                                </p>
                              </div>
                            </div>
                            <div class="col-md-2 col-12">
                              <div class="form-group">
                                <label for="itemcost">Name</label>
                                <p class="form-control-plaintext" id="itemcost">
                                  {item.name}
                                </p>
                              </div>
                            </div>

                            <div class="col-md-2 col-12">
                              <div class="form-group">
                                <label for="itemquantity">Description</label>
                                <p
                                  class="form-control-plaintext"
                                  id="itemquantity"
                                >
                                  {item.description}
                                </p>
                              </div>
                            </div>

                            <div class="col-md-2 col-12">
                              <div class="form-group">
                                <label for="staticprice">Price</label>
                                <p
                                  class="form-control-plaintext"
                                  id="staticprice"
                                >
                                  {item.price}
                                </p>
                              </div>
                            </div>

                            <div class="col-md-1 col-12 mb-50">
                              <div class="form-group">
                                <button
                                  class="btn btn-outline-danger text-nowrap px-1 waves-effect"
                                  data-repeater-delete=""
                                  type="button"
                                  onClick={() => dispatch(deleteitem(item.id))}
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="14"
                                    height="14"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    stroke="currentColor"
                                    stroke-width="2"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    class="feather feather-x mr-25"
                                  >
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                  </svg>
                                  <span>Delete</span>
                                </button>
                              </div>
                            </div>
                            <div class="col-md-1 col-12 mb-50">
                              <div class="form-group">
                                <button
                                  class="btn btn-outline-warning text-nowrap px-1 waves-effect"
                                  data-repeater-delete=""
                                  type="button"
                                  onClick={() => {
                                    const id = item.id;
                                    openUpdateModel(id);
                                  }}
                                >
                                  <span>Update</span>
                                </button>
                              </div>
                            </div>
                          </div>
                          <hr />
                        </div>
                      </div>
                    ))}
                    <div class="row">
                      <div class="col-12">
                        <button
                          class="btn btn-icon btn-primary waves-effect waves-float waves-light"
                          type="button"
                          id={category.id}
                          onClick={() => {
                            const id = category.id;
                            openModel(id);
                          }}
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="14"
                            height="14"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            class="feather feather-plus mr-25"
                          >
                            <line x1="12" y1="5" x2="12" y2="19"></line>
                            <line x1="5" y1="12" x2="19" y2="12"></line>
                          </svg>
                          <span>Add New</span>
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <hr />
    </>
  );
}

export default CategoryTable;
