import { Category } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { additem, updateitem } from "../redux/actions/itemActions";
import "../pages/Items/item.css";

function UpdateItemForm({ itemId, closeUpdate }) {
  const items = useSelector((state) => state.getitems.items);
  const [item, setItem] = useState("");
  const [inputs, setInputs] = useState({});
  const [filee, setFile] = useState("");
  const dispatch = useDispatch();
  const inputHandler = (e) => {
    if (e.target.name == "photo") {
      setInputs({
        ...inputs,
        [e.target.name]: e.target.files[0],
      });
    } else {
      setInputs({
        ...inputs,
        [e.target.name]: e.target.value,
      });
    }
  };

  const handleFile = (e) => {
    setFile(e.target.files[0]);
  };
  const onsubmit = (e) => {
    e.preventDefault();
    dispatch(updateitem(itemId, inputs));
    //set values to empty
  };
  useEffect(() => {
    items.map((Category) => {
      Category.items.map((i) => {
        if (i.id == itemId) {
          setItem(i);
        }
      });
    });
  }, []);

  return (
    <>
      <div class="container">
        <button
          class="btn-close float-end"
          onClick={() => closeUpdate()}
        ></button>
        <div class="block-heading">
          <h2 class="text-info">Add Item</h2>
          <p>
            if you want to add a new category, you can add it from the category
            section
          </p>
        </div>
        <form onSubmit={onsubmit}>
          <div class="mb-3">
            <label class="form-label" for="name">
              Name
            </label>
            <input
              id="name"
              class="form-control"
              type="text"
              name="name"
              placeholder={item.name}
              onChange={inputHandler}
            />
          </div>
          <div class="mb-3">
            <label class="form-label" for="subject">
              Description
            </label>
            <input
              id="subject"
              class="form-control"
              type="text"
              name="description"
              placeholder={item.description}
              onChange={inputHandler}
            />
          </div>
          <div class="mb-3">
            <label class="form-label" for="email">
              Price
            </label>
            <input
              id="email"
              class="form-control"
              type="number"
              name="price"
              placeholder={item.price}
              onChange={inputHandler}
            />
          </div>
          <div class="mb-3">
            <input
              class="form-control"
              name="photo"
              type="file"
              onChange={inputHandler}
            />
          </div>
          <div class="mb-3">
            <button class="btn btn-primary" type="submit">
              Update
            </button>
          </div>
        </form>
      </div>
    </>
  );
}

export default UpdateItemForm;
