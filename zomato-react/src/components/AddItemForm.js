import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { additem } from "../redux/actions/itemActions";
import "../pages/Items/item.css";
function AddItemForm({ category, close }) {
  const [inputs, setInputs] = useState({});
  const [filee, setFile] = useState("");
  const dispatch = useDispatch();
  const inputHandler = (e) => {
    setInputs({
      ...inputs,
      [e.target.name]: e.target.value,
    });
  };
  const handleFile = (e) => {
    setFile(e.target.files[0]);
    console.log(filee);
  };
  const onsubmit = (e) => {
    e.preventDefault();
    const cat = parseInt(inputs.category);
    dispatch(
      additem(inputs.name, inputs.description, inputs.price, category, filee)
    );
    //set values to empty
    setInputs({
      name: "",
      description: "",
      price: "",
      category: "",
    });
    //set file to empty
    setFile("");
  };

  return (
    <div>
      <button class="btn-close float-end" onClick={() => close()}></button>
      <div class="block-heading">
        <h2 class="text-info">Add Item</h2>
        <p>
          if you want to add a new category, you can add it from the category
          section
        </p>
      </div>
      <form onSubmit={onsubmit}>
        <div class="mb-3">
          <label class="form-label" for="name">
            Name
          </label>
          <input
            id="name"
            class="form-control"
            type="text"
            name="name"
            value={inputs.name}
            onChange={inputHandler}
          />
        </div>
        <div class="mb-3">
          <label class="form-label" for="subject">
            Description
          </label>
          <input
            id="subject"
            class="form-control"
            type="text"
            name="description"
            value={inputs.description}
            onChange={inputHandler}
          />
        </div>
        <div class="mb-3">
          <label class="form-label" for="email">
            Price
          </label>
          <input
            id="email"
            class="form-control"
            type="number"
            name="price"
            filename={filee}
            onChange={inputHandler}
          />
        </div>
        <div class="mb-3">
          <input class="form-control" type="file" onChange={handleFile} />
        </div>
        <div class="mb-3">
          <button class="btn btn-primary" type="submit">
            Save
          </button>
        </div>
      </form>
    </div>
  );
}

export default AddItemForm;
