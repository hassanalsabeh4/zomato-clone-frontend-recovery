import { combineReducers } from "redux";
import { OrderReducer } from "./orderReduce";

import { categoryReducer } from "./categoryReducer";
import { restaurantReducer } from "./restaurantReducer";
import { DriverReducer } from "./driverReducer";
import itemReducer from "./itemReducer";
const reducers = combineReducers({
  data: restaurantReducer,
  orders: OrderReducer,
  getitems: itemReducer,
  categories: categoryReducer,
  drivers: DriverReducer,
});

export default reducers;
