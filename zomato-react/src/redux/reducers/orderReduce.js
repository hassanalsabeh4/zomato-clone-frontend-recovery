import { ActionTypes } from "../contants/action-types";

const initialState = {
  orders: [],
  order:{}
};

export const OrderReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.FETCH_ORDERS:
      return {
        orders: payload,
      };
    case ActionTypes.EDIT_ORDERS:
      return {
        orders: payload,
      };
      case ActionTypes.FETCH_ORDER:
        debugger
      return {
        order: payload,
      };
    default:
      return state;
  }
};
