import React from "react";
import { ActionTypes } from "../contants/action-types";
const InitialState = {
  items: [],
};
function itemReducer(state = InitialState, { type, payload }) {
  if (type == ActionTypes.GET_ITEM) {
    return { ...state, items: payload };
  }
  if (type == ActionTypes.DELETE_ITEM) {
    // const newArray=state.items..restaurant_category.filter(i=>i.payload !==id);
    // console.log(newArray)
    console.log("payload", payload);
    return { ...state, items: payload };
  }
  if (type == ActionTypes.UPDATE_ITEM) {
    return { ...state, items: payload };
  }
  if (type == ActionTypes.ADD_ITEM) {
    return { ...state, items: payload };
  }

  return state;
}

export default itemReducer;
