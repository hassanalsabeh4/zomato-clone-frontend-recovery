import { ActionTypes } from "../contants/action-types";

const initialState = {
  categories: [],
};

export const categoryReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.FETCH_CATEGORY:
      return {
        ...state,
        categories: payload,
      };
    case ActionTypes.GET_CATEGORIES:
      return { ...state, categories: payload };
    case ActionTypes.ADD_CATEGORY:
      return {
        ...state,
        categories: payload,
      };
    default:
      return state;
  }
};
