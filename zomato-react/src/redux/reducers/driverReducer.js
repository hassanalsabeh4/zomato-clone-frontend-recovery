import { ActionTypes } from "../contants/action-types";

const initialState = {
  drivers: [],
  driverz: [],
};

export const DriverReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.FETCH_DRIVERS:
      return {
        drivers: payload,
      };
    case ActionTypes.ADD_DRIVERS:
      return {
        driverz: payload,
      };
    case ActionTypes.EDIT_DRIVERS:
      return {
        drivers: payload,
      };
    default:
      return state;
  }
};
