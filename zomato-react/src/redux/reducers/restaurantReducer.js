import { ActionTypes } from "../contants/action-types";

const initialState = {
  restaurants: [],
  restaurant: {},
};

export const restaurantReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.FETCH_RESTAURANT:
      debugger
      return {
        restaurant: payload,
      };
    case ActionTypes.DELETE_RESTAURANT:

      return {
        restaurant: payload,
      };
    case ActionTypes.EDIT_RESTAURANT:
      debugger
      return {
        restaurant: payload,
      };
    case ActionTypes.LOGIN_RESTAURANT:
      debugger
      return {
        restaurant: payload,
      };
    case ActionTypes.ADD_RESTAURANT:
      return {
        restaurant: payload,
      };
    default:
      return state;
  }
};
