import urlAxios from "../../apis/axiosApi";
import { ActionTypes } from "../contants/action-types";

export const fetchOrders = () => {
  const id = localStorage.getItem("id");
  console.log("restaurent id", id);
  return async function (dispatch) {
    try {
      const response = await urlAxios.get(`/getorders?restaurant=${id}`);
      const data = response.data.data;
      console.log("order1:::::", data);
      debugger
      dispatch({ type: ActionTypes.FETCH_ORDERS, payload: data });
    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
      }
      console.log(err.message);
    }
  };
};

export const fetchOrdersById = (id) => {

  return async function (dispatch) {
    try {
      const response = await urlAxios.get(`/getorder/${id}`);
      const data = response.data;
      console.log("order1:::::", data);
      debugger
      dispatch({ type: ActionTypes.FETCH_ORDER, payload: data });
    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
      }
      console.log(err.message);
    }
  };
};

export const removeOrder = (id) => async (dispatch, getState) => {
  try {
    const response = await urlAxios.delete(`/delete_order/${id}`);
    const data = response.data;
    console.log("data", data);
    if (data.success) {
      const newData = getState().orders.orders.filter(
        (item) => item.id != id
      );
      dispatch({ type: ActionTypes.FETCH_ORDERS, payload: newData });
    }
  } catch (err) {
    if (err.response && !err.response.success) {
      console.log(err.response.message);
    } else {
      console.log(err.message);
    }
  }
};


export const updateOrder= (object, id, navigate) => {

  return async function (dispatch, getState) {
    try {
      const request = new FormData();
      if (object.is_paid) {
        request.append("is_paid", object.is_paid);
      }
      if (object.total_price) {
        request.append("total_price", object.total_price);
      }
    

      const response = await urlAxios.post(
        `/update_order/${id}?_method=put`,
        request
      );
      const data = response.data;

      if (data.success) {
        let newData = getState().data.restaurants.filter((item) => {
          if (item.id === data.data.id) {
            return data.data;
          }
          return item;
        });
        dispatch({ type: ActionTypes.EDIT_ORDERS, payload: newData });
        navigate("/orders");
      }
    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
      }
      console.log(err.message)
    }
  };
};

