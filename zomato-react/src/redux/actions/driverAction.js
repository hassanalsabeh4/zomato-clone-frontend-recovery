import Swal from "sweetalert2";
import urlAxios from "../../apis/axiosApi";
import { ActionTypes } from "../contants/action-types";

export const fetchDriver = () => {
  const id = localStorage.getItem("id");

  return async function (dispatch) {
    try {
      const response = await urlAxios.get(`/getdrivers?restaurant=${id}`);
      const data = response.data.data;

      dispatch({ type: ActionTypes.FETCH_DRIVERS, payload: data });
    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
      }
      console.log(err.message);
    }
  };
};

export const addDriver = (value, navigate) => {
  return async function (dispatch) {
    try {
      const response = await urlAxios.post(`/adddriver`, value, {
        headers: {
          "Content-type": "application/json",
          Accept: "application/json",
        },
      });
      debugger;
      const data = response.data;

      if (data.success) {
        dispatch({ type: ActionTypes.ADD_DRIVERS, payload: data.data });
        navigate("/drivers");
        Swal.fire("Good job!", "Driver added successfully", "success");
      }
    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Wrong details! Try Again",
        });
      }
      console.log(err.message);
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Some error occured",
      });
    }
  };
};

export const removeDriver = (id) => async (dispatch, getState) => {
  try {
    const response = await urlAxios.delete(`/delete_driver/${id}`);
    const data = response.data;
    console.log("data", data);
    if (data.success) {
      const newData = getState().drivers.drivers.filter(
        (item) => item.id != id
      );
      dispatch({ type: ActionTypes.FETCH_DRIVERS, payload: newData });
    }
  } catch (err) {
    if (err.response && !err.response.success) {
      console.log(err.response.message);
    } else {
      console.log(err.message);
    }
  }
};

export const updateDriver = (object, id, navigate) => {

  return async function (dispatch, getState) {
    try {
      const request = new FormData();
      if (object.name) {
        request.append("name", object.name);
      }
      if (object.email) {
        request.append("email", object.email);
      }
      if (object.password) {
        request.append("password", object.password);
      }
      if (object.phone) {
        request.append("phone", object.phone);
      }

      const response = await urlAxios.post(
        `/update_driver/${id}?_method=put`,
        request
      );
      const data = response.data;

      if (data.success) {
        let newData = getState().data.restaurants.filter((item) => {
          if (item.id === data.data.id) {
            return data.data;
          }
          return item;
        });
        dispatch({ type: ActionTypes.EDIT_DRIVERS, payload: newData });
        navigate("/drivers");
      }
    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
      }
      console.log(err.message)
    }
  };
};
