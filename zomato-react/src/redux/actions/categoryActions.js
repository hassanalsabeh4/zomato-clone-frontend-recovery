import React from "react";
import urlAxios from "../../apis/axiosApi";
import { ActionTypes } from "../contants/action-types";
export const getcategories = (id) => async (dispatch) => {
  try {
    // const response = await axiosApi.get(`CategoriesByResturant/${id}`);
    const response = await urlAxios.get(
      `CategoriesByResturant/${localStorage.getItem("id")}`
    );
    const result = response.data;
    dispatch({
      type: ActionTypes.GET_CATEGORIES,
      payload: result,
    });
  } catch (e) {
    console.log("error");
  }
};

export const fetchCategory = () => async (dispatch, getState) => {
  try {
    const response = await urlAxios.get(`/restaurant/category`);
    const data = response.data;
    if (data.success) {
      dispatch({ type: ActionTypes.FETCH_CATEGORY, payload: data.data });
    }
  } catch (err) {
    if (err.response && !err.response.success) {
      console.log(err.response.message);
    } else {
      console.log(err.message);
    }
  }
};

export const addCategory = (object, navigate) => async (dispatch, getState) => {
  try {
    const form = new FormData();
    form.append("name", object.name);
    form.append("restaurant_id", localStorage.getItem("id"));
    const response = await urlAxios.post(`/restaurant/category`, form);
    const data = response.data;
    if (data.success) {
      const newData = [...getState().categories.categories, data.data];

      dispatch({ type: ActionTypes.ADD_CATEGORY, payload: newData });
      navigate("/categories");
    }
  } catch (err) {
    if (err.response && !err.response.success) {
      console.log(err.response.message);
    } else {
      console.log(err.message);
    }
  }
};

export const updateCategory =
  (object, id, navigate) => async (dispatch, getState) => {
    try {
      const form = new FormData();
      if (object.name) {
        form.append("name", object.name);
      }
      const response = await urlAxios.post(
        `/restaurant/category/${id}?_method=put`,
        form
      );
      const data = response.data;
      if (data.success) {
        const newData = getState().categories.categories.filter((item) => {
          if (item.id == id) {
            return data.data;
          }
          return item;
        });
        dispatch({ type: ActionTypes.ADD_CATEGORY, payload: newData });
        navigate("/categories");
      }
    } catch (err) {
      if (err.response && !err.response.success) {
        console.log(err.response.message);
      } else {
        console.log(err.message);
      }
    }
  };

export const deleteCategory = (id, navigate) => async (dispatch, getState) => {
  try {
    const response = await urlAxios.delete(`/restaurant/category/${id}`);
    const data = response.data;
    if (data.success) {
      const newData = getState().categories.categories.filter(
        (item) => item.id != id
      );
      dispatch({ type: ActionTypes.ADD_CATEGORY, payload: newData });
      navigate("/categories");
    }
  } catch (err) {
    if (err.response && !err.response.success) {
      console.log(err.response.message);
    } else {
      console.log(err.message);
    }
  }
};
