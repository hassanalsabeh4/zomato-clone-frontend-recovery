import React from "react";
import { ActionTypes } from "../contants/action-types";
import axiosApi from "../../apis/axiosApi";
import { data } from "jquery";
import { Category } from "@material-ui/icons";
export const getitems = (id) => async (dispatch) => {
  try {
    const response = await axiosApi.get(
      `restaurant/details/${localStorage.getItem("id")}`
    );
    const result = response.data.data;
    dispatch({
      type: ActionTypes.GET_ITEM,
      payload: result.restaurant_category,
    });
  } catch (e) {
    console.log("catch error:", "error");
  }
};

export const deleteitem = (id) => async (dispatch, getState) => {
  try {
    const response = await axiosApi.delete(`item/delete/${id}`);
    const result = response.data;
    if (result.success) {
      console.log("items", getState().getitems.items);

      const newData = getState().getitems.items.map((category) => ({
        ...category,
        items: category.items.filter((item) => item.id !== id),
      }));
      dispatch({ type: ActionTypes.DELETE_ITEM, payload: newData });
    } else {
      console.log("error");
    }
  } catch (e) {
    console.log("Somthing wrong! please try again later ");
  }
};

export const additem =
  (name, description, price, category, photo) => async (dispatch, getState) => {
    try {
      const data = new FormData();
      data.append("name", name);
      data.append("description", description);
      data.append("price", price);
      data.append("categorie_id", category);
      data.append("photo", photo);
      data.append("restaurant_id", localStorage.getItem("id"));
      const response = await axiosApi.post("/item/additem", data);
      const result = response.data;
      if (result.success) {
        alert("item added successfully");
        const newData = getState().getitems.items.map((category) => {
          if (category.id == result.data.categorie_id) {
            return {
              ...category,
              items: [...category.items, result.data],
            };
          } else {
            return { ...category };
          }
        });

        dispatch({ type: ActionTypes.ADD_ITEM, payload: newData });
      } else {
        console.log("error from else", result.errors[0]);
      }
    } catch (e) {
      console.log("error from catch", e.message);
    }
  };
export const updateitem = (id, obj) => async (dispatch, getState) => {
  try {
    const data = new FormData();
    console.log("first data", data);

    //function that take object and append to formdata
    const appendFormData = (obj) => {
      for (let key in obj) {
        console.log("key", key);
        console.log("obj[key]", obj[key]);
        data.append(key, obj[key]);
      }
    };
    appendFormData(obj);
    const response = await axiosApi.post(
      `/item/update/${id}?_method=put`,
      data
    );
    const result = response.data;
    if (result.success) {
      alert("item updated successfully");
      const newData = getState().getitems.items.map((category) => ({
        ...category,
        items: category.items.map((item) => {
          if (item.id == id) {
            return result.data;
          } else {
            return item;
          }
        }),
      }));
      console.log("new data araaaay ", newData);
      dispatch({ type: ActionTypes.UPDATE_ITEM, payload: newData });
    }
  } catch (e) {
    console.log("errrorrr", e);
  }
};
