import urlAxios from '../../apis/axiosApi';
import { ActionTypes } from '../contants/action-types';
import Swal from "sweetalert2";

export const fetchRestaurants = () => {
  return async function (dispatch) {
    try {
      const response = await urlAxios.get(`/restaurant/details/${localStorage.getItem("id")}`);
      const data = response.data.data;
      debugger
      dispatch({ type: ActionTypes.FETCH_RESTAURANT, payload: data })
    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
      }
      console.log(err.message)
    }
  }
}

export const updateRestaurants = (object, navigate) => {
  return async function (dispatch, getState) {
    try {
      const request = new FormData();
      if (object.name) {
        request.append("name", object.name)

      }
      if (object.email) {
        request.append("email", object.email)

      }

      if (object.mobile) {
        request.append("mobile", object.mobile)
      }

      if (object.tel) {
        request.append("tel", object.tel)

      }
      if (object.address) {
        request.append("address", object.address)

      }

      if (object.description) {
        request.append("description", object.description)

      }

      const response = await urlAxios.post(`/restaurant/details/${localStorage.getItem("id")}?_method=put`, request);
      const data = response.data;
      if (data.success) {

        dispatch({ type: ActionTypes.EDIT_RESTAURANT, payload: data.data })
        debugger
        navigate("/show-restaurants");
      }
    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
      }
      console.log(err.message)
    }
  }
}


export const loginRestaurants = (object, navigate) => {
  return async function (dispatch) {
    try {

      const response = await urlAxios.post('/restaurant/login', object, {
        headers: {
          "Content-type": "application/json",
          "Accept": "application/json"
        }
      });
      const data = response.data;
      if (data.success) {
        localStorage.setItem("token", data.access_token);
        localStorage.setItem("id", data.data.id);
        debugger
        dispatch({ type: ActionTypes.LOGIN_RESTAURANT, payload: data.data })
        navigate("/show-restaurants");
        Swal.fire("Good job!", "Logged in successfully", "success");
      }
    } catch (err) {
      if (err.response) {

        console.log(err.response.message);
      }
      console.log(err.message)

      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Wrong Username or Password login!   Try Again',
      })
    }
  }
}

export const addRestaurants = (object, navigate) => {
  return async function (dispatch) {
    try {

      const response = await urlAxios.post(
        `/restaurant/register`,
        object,
        {
          headers: {
            "Content-type": "application/json",
            "Accept": "application/json"
          }
        }
      );
      const data = response.data;
      if (data.success) {
        dispatch({ type: ActionTypes.ADD_RESTAURANT, payload: data.data })
        navigate("/");
        Swal.fire("Good job!", "Registered successfully", "success");
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Wrong details! Try Again',
        })
      }

    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Wrong details! Try Again',
        })
      }
      console.log(err.message)
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Some error occured',
      })
    }
  }
}

export const deleteRestaurant = (id) => {
  return async function (dispatch, getState) {
    try {
      const response = await urlAxios.delete(`/restaurant/details/${id}`);
      const data = response.data;
      if (data.success) {
        ;
        let newData = getState().data.restaurants.filter((item) => item.id != data.data);
        ;
        dispatch({ type: ActionTypes.DELETE_RESTAURANT, payload: newData })

      }
    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
      }
      console.log(err.message)
    }
  }
}