import React from "react";
import { useNavigate } from "react-router";
import "./dashboard.css";

const Dashboard = () => {
  const navigate = useNavigate();
  return (
    <div>
      <div className="area"></div>
      <nav className="main-menu">
        <ul>
          <li>
            <a href="#">
              <i className="fa fa-home fa-2x"></i>
              <span className="nav-text">Dashboard</span>
            </a>
          </li>
          <li className="has-subnav">
            <a href="" onClick={() => navigate("/show-restaurants")}>
              <i class="fa fa-cutlery"></i>
              <span className="nav-text">Restaurants</span>
            </a>
          </li>
          <li className="has-subnav">
            <a href="" onClick={() => navigate("/categories")}>
              <i className="	fa fa-list-alt"></i>
              <span className="nav-text">Categories</span>
            </a>
          </li>
          <li className="has-subnav">
            <a href="/items">
              <i className="fa fa-list"></i>
              <span className="nav-text">Items</span>
            </a>
          </li>
          <li className="has-subnav">
            <a href="/orders">
              <i className="fa fa-shopping-cart"></i>
              <span className="nav-text">Orders</span>
            </a>
          </li>
          <li className="has-subnav">
            <a href="/drivers">
              <i className="fa fa-user fa-2x"></i>
              <span className="nav-text">Drivers</span>
            </a>
          </li>
        </ul>
        <ul className="logout">
          <li onClick={() => {
            localStorage.removeItem("id")
            localStorage.removeItem("token")
            navigate("/")
          }}>
            <a href="">
              <i className="fa fa-power-off fa-2x"></i>
              <span className="nav-text">Logout</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Dashboard;
