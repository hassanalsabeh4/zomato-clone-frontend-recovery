import React, { useEffect, useState } from "react";
import Dashboard from "../dashboard/dashboard";
import { Table } from "react-bootstrap";
import Modal from "react-modal";
import Button from "react-bootstrap/Button";
import { useDispatch, useSelector } from "react-redux";
import { getitems } from "../../redux/actions/itemActions";
import { getanwar } from "../../redux/actions/itemActions";
import TableData from "../../components/TableData";
import AddItemForm from "../../components/AddItemForm";
import { getcategories } from "../../redux/actions/categoryActions";
import CategoryTable from "../../components/CategoryTable";
import "../Items/item.css";
import "../login/login.css";
import UpdateItemForm from "../../components/UpdateItemForm";
import { Style } from "@material-ui/icons";
import { style } from "dom-helpers";
export default function Items() {
  const [modal, setModal] = useState(false);
  const [updateModal, setUpdateModal] = useState(false);
  const dispatch = useDispatch();
  const [selectedCat, setSelectedCat] = useState("");
  const [selectedItem, setSelectedItem] = useState("");
  const items = useSelector((state) => state.getitems.items);
  const categories = useSelector((state) => state.categories.categories);
  console.log("itemssssss", items);

  const openUpdateModel = (e) => {
    setUpdateModal(true);
    setSelectedItem(e);
  };
  const openModel = (e) => {
    setModal(true);
    setSelectedCat(e);
    console.log("e", e);
  };
  function close() {
    setModal(false);
    setSelectedCat("");
  }
  function closeUpdate() {
    setUpdateModal(false);
    setSelectedItem("");
  }
  useEffect(() => {
    dispatch(getitems());
    dispatch(getcategories(1));
  }, []);
  return (
    <div style={modal || updateModal ? { visibility: "hidden" } : null}>
      <Dashboard />
      <div className="container">
        <div class="page-header">
          <h1> Add items page</h1>
        </div>
        <p> Add,edit or delete items.</p>
        <Modal isOpen={modal} className="model fadeInDown">
          <AddItemForm category={selectedCat} close={close} />
        </Modal>
        <Modal isOpen={updateModal} className="model fadeInDown">
          <UpdateItemForm itemId={selectedItem} closeUpdate={closeUpdate} />
        </Modal>
        {items.map((category) => (
          <CategoryTable
            category={category}
            openModel={openModel}
            openUpdateModel={openUpdateModel}
          />
        ))}
      </div>
    </div>
  );
}
