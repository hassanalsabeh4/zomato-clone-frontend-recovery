import React, { useEffect } from "react";
import Dashboard from "../dashboard/dashboard";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router"
import { fetchRestaurants } from "../../redux/actions/restaurantActions";

export default function ShowRestaurants() {

  const restaurant = useSelector((state) => state.data.restaurant);
  debugger
  let dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(fetchRestaurants());
  }, [])

  return (
    <>
      <Dashboard />
      <div className="d-flex  flex-lg-column justify-content-center align-items-center">
        <div className="col-md-4 mb-3">
          <div className="card">
            <div className="card-body">
              <div className="d-flex flex-column align-items-center text-center">
                <img src={restaurant && restaurant.restaurant_info && restaurant.restaurant_info.logo ? restaurant.restaurant_info.logo : "https://bootdey.com/img/Content/avatar/avatar7.png"} alt="Admin" className="rounded-circle" width="150" />
                <div className="mt-3">
                  <h4>{restaurant && restaurant.name}</h4>
                  <p className="text-muted font-size-sm">{restaurant && restaurant.restaurant_info && restaurant.restaurant_info.address}</p>
                  <button onClick={() => navigate(`/edit-restaurants/${localStorage.getItem("id")}`)} className="btn btn-outline-primary w-100">EDIT</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-8 mx-auto">
        <div className="card mb-3">
          <div className="card-body">
            <div className="row">
              <div className="col-sm-3">
                <h6 className="mb-0">Name</h6>
              </div>
              <div className="col-sm-9 text-secondary">
                {restaurant && restaurant.name}
              </div>
            </div>
            <hr />
            <div className="row">
              <div className="col-sm-3">
                <h6 className="mb-0">Email</h6>
              </div>
              <div className="col-sm-9 text-secondary">
                {restaurant && restaurant.email}
              </div>
            </div>
            <hr />
            <div className="row">
              <div className="col-sm-3">
                <h6 className="mb-0">Phone</h6>
              </div>
              <div className="col-sm-9 text-secondary">
                {restaurant && restaurant.restaurant_info && restaurant.restaurant_info.tel}
              </div>
            </div>
            <hr />
            <div className="row">
              <div className="col-sm-3">
                <h6 className="mb-0">Mobile</h6>
              </div>
              <div class="col-sm-9 text-secondary">
                {restaurant && restaurant.restaurant_info && restaurant.restaurant_info.mobile}
              </div>
            </div>
            <hr />
            <div className="row">
              <div className="col-sm-3">
                <h6 className="mb-0">Address</h6>
              </div>
              <div className="col-sm-9 text-secondary">
                {restaurant && restaurant.restaurant_info && restaurant.restaurant_info.address}
              </div>
            </div>
            <hr />
          </div>
        </div>
      </div>
    </>
  );
}
