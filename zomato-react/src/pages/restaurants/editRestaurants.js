import React, { useEffect, useState } from 'react';
import Dashboard from "../dashboard/dashboard";
import { useDispatch, useSelector } from "react-redux";
import { fetchRestaurants, updateRestaurants } from '../../redux/actions/restaurantActions';
import { useNavigate, useParams } from 'react-router';

const EditRestaurants = () => {
  let { id } = useParams();
  const editRestaurant = useSelector((state) => state.data.restaurant);
  const [editData, setEditData] = useState({})
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const onChange = (e) => {
    const { name, value } = e.target;

    setEditData((prev => {
      return {
        ...prev,
        [name]: value
      }
    }))

  }



  const submit = (e) => {
    e.preventDefault();
    dispatch(updateRestaurants(editData, navigate));

  }

  useEffect(() => {
    dispatch(fetchRestaurants());
  }, [])
  return (
    <>
      <Dashboard />
      <div className="d-flex flex-lg-column justify-content-center align-items-center vh-100 text-center">
        <h1 className="mb-3">EDIT RESTAURANT</h1>
        <div className="w-50">
          <form className="" onSubmit={submit}>
            <div class="form-group mb-3">
              <input
                type="text"
                id="name"
                class="form-control p-3"
                name="name"
                placeholder="Name"
                defaultValue={editRestaurant && editRestaurant.name}
                onChange={onChange}
              />
            </div>
            <div class="form-group mb-3">
              <input
                type="text"
                id="email"
                class="form-control p-3"
                name="email"
                placeholder="email"
                defaultValue={editRestaurant && editRestaurant.email}
                onChange={onChange}
              />
            </div>
            <div class="form-group mb-3">
              <input
                type="text"
                id="description"
                class="form-control p-3"
                name="description"
                placeholder="description"
                defaultValue={editRestaurant && editRestaurant.restaurant_info && editRestaurant.restaurant_info.description}
                onChange={onChange}
              />
            </div>
            <div class="form-group mb-3">
              <input
                type="text"
                id="mobile"
                class="form-control p-3"
                name="mobile"
                placeholder="mobile"
                defaultValue={editRestaurant && editRestaurant.restaurant_info && editRestaurant.restaurant_info.mobile}

                onChange={onChange}
              />
            </div>
            <div class="form-group mb-3">
              <input
                type="text"
                id="tel"
                class="form-control p-3"
                name="tel"
                placeholder="tel"
                defaultValue={editRestaurant && editRestaurant.restaurant_info && editRestaurant.restaurant_info.tel}

                onChange={onChange}
              />
            </div>
            <div class="form-group mb-3">
              <input
                type="text"
                id="address"
                class="form-control p-3"
                name="address"
                placeholder="address"
                defaultValue={editRestaurant && editRestaurant.restaurant_info && editRestaurant.restaurant_info.address}
                onChange={onChange}
              />
            </div>
            <input type="submit" className="d-block fadeIn fourth m-0 mt-3" value="Update" />
          </form>
        </div>
      </div>
    </>
  );
}

export default EditRestaurants;