/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import "./login.css"
import { useDispatch } from "react-redux";
import { loginRestaurants } from "../../redux/actions/restaurantActions";
import { useNavigate } from "react-router";
function Login1() {
  document.title = "Login";
  let dispatch = useDispatch();
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  async function login() {
    let item = { email, password };
    dispatch(loginRestaurants(item, navigate))
  }

  return (
    <div className="wrapper fadeInDown">
      <div id="formContent">
        <h2 className="active"> Sign In </h2>

        <div className="fadeIn first">
          <img
            src="https://s3-symbol-logo.tradingview.com/yum-brands--600.png"
            height="140px"
            id="icon"
            alt="User Icon"
          />
        </div>

        <br></br>
        <input
          type="text1"
          id="login"
          className="fadeIn second"
          name="email"
          placeholder="Email"
          required
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          type="password"
          id="password"
          className="fadeIn third"
          name="password"
          placeholder="password"
          required
          onChange={(e) => setPassword(e.target.value)}
        />
        <input
          type="submit"
          onClick={login}
          className="fadeIn fourth"
          value="Log In"
        />

        <div id="formFooter">
          <a onClick={() => navigate("/register")} className="underlineHover" href="">
            Don't have an account?  Register
          </a>
        </div>
      </div>
    </div>
  );
}
export default Login1;
