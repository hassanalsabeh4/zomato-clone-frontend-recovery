import React, { useEffect, useState } from "react";
import Dashboard from "../dashboard/dashboard";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import { updateDriver } from "../../redux/actions/driverAction";

const EditDriver = () => {
  const [inputValue, setInputValue] = useState({
    name: "",
    email: "",
    password: "",
    phone: "",
  });
  const [driver, setDriver] = useState({});

  let { id } = useParams();

  const drivers = useSelector((state) => state.drivers.drivers);
  useEffect(() => {
    setDriver(drivers.filter((item) => item.id == id)[0]);
  }, [drivers]);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handelSubmit = (e) => {
    e.preventDefault();
    console.log(inputValue);
    dispatch(updateDriver(inputValue, id, navigate));
  };

  const handelInput = (e) => {
    e.persist();
    setInputValue({ ...inputValue, [e.target.name]: e.target.value });
    console.log(inputValue);
  };

  return (
    <>
      <Dashboard />
      <br />
      <h1 className="fadeIn third" style={{ margin: "5% 20% 0 20%" }}>
        Edit Driver
      </h1>
      
      <form
        //className="col-md-9 mx-auto"
        className="form-wrapper"
        style={{ margin: "2% 20% 0 20%" }}
        onSubmit={handelSubmit}
      >
        <input
          type="text"
          className="form-control fadeIn third"
          name="name"
          placeholder="Name"
          defaultValue={driver && driver.name}
          onChange={handelInput}
        />
        <br />
        <input
          type="text"
          className="form-control fadeIn third"
          name="email"
          placeholder="Email"
          defaultValue={driver && driver.email}
          onChange={handelInput}
        />
        <br />
        <input
          type="text"
          className="form-control fadeIn third"
          name="password"
          placeholder="password"
          defaultValue={driver && driver.password}
          onChange={handelInput}
        />
        <br />
        <input
          type="text"
          //className="fadeIn third"
          className="form-control fadeIn third"
          name="phone"
          placeholder="phone"
          defaultValue={driver && driver.phone}
          onChange={handelInput}
        />
        <br />
        <input type="submit" className="d-block fadeIn fourth" value="Update" />
      </form>
    </>
  );
};

export default EditDriver;
