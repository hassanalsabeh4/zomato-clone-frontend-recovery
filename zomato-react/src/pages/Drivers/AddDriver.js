import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { addDriver } from "../../redux/actions/driverAction";
export default function AddDriver() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  function addDriverz() {
    const data = { name, email, password, phone, restaurant_id };
    dispatch(addDriver(data, navigate));
  }
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [phone, setPhone] = useState("");
  const [restaurant_id, setRestaurant_id] = useState(
    localStorage.getItem("id")
  );
  return (
    <div>
      <div className="form-wrapper" style={{ margin: "5% 20% 0 20%" }}>
        <br />
        <h1>Add Driver</h1>
        <br />
        {/* {error ? (
          <span style={{ color: "red" }}> Team name already in use. </span>
        ) : null} */}
        <input
          type="text"
          onChange={(e) => setName(e.target.value)}
          className="form-control"
          placeholder="name"
          id="name"
        />
        <br />
        <input
          type="text"
          onChange={(e) => setEmail(e.target.value)}
          className="form-control"
          placeholder="email"
          id="email"
        />
        <br />
        <input
          type="text"
          onChange={(e) => setPassword(e.target.value)}
          className="form-control"
          placeholder="password"
          id="password"
        />
        <br />

        <input
          type="text"
          onChange={(e) => setPhone(e.target.value)}
          className="form-control"
          placeholder="phone"
          id="phone"
        />
        <br />
        <br />
      </div>
      <br />
      <br />
      <div className="col-sm-6 offset-sm-3">
        <button
          className="btn btn-primary"
          onClick={addDriverz}
          style={{
            marginTop: "-10%",
            marginLeft: "-10%",
            width: "150px",
            height: "50px",
          }}
        >
          Add Driver
        </button>
      </div>
    </div>
  );
}
