import React, { useEffect } from "react";
import Dashboard from "../dashboard/dashboard";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
import { useDispatch, useSelector } from "react-redux";
import { fetchDriver, removeDriver } from "../../redux/actions/driverAction";
import { useNavigate } from "react-router";

export default function Drivers() {
  const drivers = useSelector((state) => state.drivers.drivers);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(fetchDriver());
  }, []);
  console.log("order2::::::", drivers);

  // const handelDelete = (id) => {
  //   dispatch(removeDriver(id));
  //   dispatch(fetchDriver());
  // };

  return (
    <div>
      <Dashboard />
      <div style={{ margin: "5% 5% 0 20%" }}>
        {/* <a href=""> */}
        <Button
          variant="success"
          size="lg"
          block="block"
          type="submit"
          style={{ margin: "2% 0 2% 0" }}
          onClick={() => navigate("/add-driver")}
        >
          Add Drivers
        </Button>
        {/* </a> */}
        <Table hover className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">phone</th>
              <th scope="col">is_available</th>

              <th style={{ width: "80px" }}>Delete</th>
              <th style={{ width: "80px" }}>Edit</th>
            </tr>
          </thead>
          {drivers &&
            drivers.map((val) => (
              <tbody>
                <tr>
                  <td>{val.name}</td>
                  <td>{val.email}</td>
                  <td>{val.phone}</td>
                  <td>{val.is_available}</td>
                  <td>
                    <Button
                      size="sm"
                      variant="danger"
                      onClick={() => dispatch(removeDriver(val.id))}
                    >
                      Delete
                    </Button>
                  </td>
                  <td>
                    <Button
                      size="sm"
                      variant="info"
                      style={{ marginRight: "10px" }}
                      onClick={() => navigate(`/edit-driver/${val.id}`)}
                    >
                      Edit
                    </Button>
                  </td>
                </tr>
              </tbody>
            ))}
        </Table>
      </div>
    </div>
  );
}
