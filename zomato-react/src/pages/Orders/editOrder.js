import React, { useEffect, useState } from "react";
import Dashboard from "../dashboard/dashboard";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import { updateOrder } from "../../redux/actions/orderAction";

const EditOrder = () => {
  const [inputValue, setInputValue] = useState({
    is_paid: "",
    total_price: "",
    
  });
  const [order, setOrder] = useState({});

  let { id } = useParams();

  const orders = useSelector((state) => state.orders.orders);
  useEffect(() => {
    setOrder(orders.filter((item) => item.id == id)[0]);
  }, [order]);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handelSubmit = (e) => {
    e.preventDefault();
    console.log(inputValue);
    dispatch(updateOrder(inputValue, id, navigate));
  };

  const handelInput = (e) => {
    e.persist();
    setInputValue({ ...inputValue, [e.target.name]: e.target.value });
    console.log(inputValue);
  };

  return (
    <>
      <Dashboard />
      <br />
      <h1 className="fadeIn third" style={{ margin: "5% 20% 0 20%" }}>
        Edit Order
      </h1>

      <form
        //className="col-md-9 mx-auto"
        className="form-wrapper"
        style={{ margin: "2% 20% 0 20%" }}
        onSubmit={handelSubmit}
      >
        <input
          type="text"
          className="form-control fadeIn third"
          name="is_paid"
          placeholder="is_paid"
          defaultValue={order && order.is_paid}
          onChange={handelInput}
        />
        <br />
        <input
          type="text"
          className="form-control fadeIn third"
          name="total_price"
          placeholder="Total price"
          defaultValue={order && order.total_price}
          onChange={handelInput}
        />

        <br />
        <input type="submit" className="d-block fadeIn fourth" value="Update" />
      </form>
    </>
  );
};

export default EditOrder;
