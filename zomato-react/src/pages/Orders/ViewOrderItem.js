import { Table } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { fetchOrdersById } from "../../redux/actions/orderAction";
import Dashboard from "../dashboard/dashboard";

const ViewOrderItem = () => {
  const dispatch = useDispatch();
  const { id } = useParams();
  const order = useSelector((state) => state.orders.order);
  debugger;

  useEffect(() => {
      debugger
    dispatch(fetchOrdersById(id));
  }, [id]);

  return (
    <div style={{ margin: "5% 5% 0 20%" }}>
        <h1>Items per order</h1>
        <Dashboard />
      <Table hover className="table">
        <thead className="thead-dark">
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Quantity</th>
            <th scope="col">Price</th>
          </tr>
        </thead>
        {order && order.item &&
          order.item.map((val) => (
            <tbody>
              <tr>
                <th scope="row">{val && val.itemzz.id}</th>

                <td>{val && val.itemzz.name}</td>
                <td>{val && val.quantity}</td>
                <td>{val.price}</td>
              </tr>
            </tbody>
          ))}
      </Table>
    </div>
  );
};

export default ViewOrderItem;
