import React, { useEffect } from "react";
import Dashboard from "../dashboard/dashboard";
import { Table } from "react-bootstrap";

import Button from "react-bootstrap/Button";
import { useDispatch, useSelector } from "react-redux";
import { fetchOrders, removeOrder } from "../../redux/actions/orderAction";
import { useNavigate } from "react-router";

export default function Orders() {
  const orders = useSelector((state) => state.orders.orders);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    dispatch(fetchOrders());
  }, []);

  return (
    <div>
      <Dashboard />
      <div style={{ margin: "5% 5% 0 20%" }}>
        {/* <a href=""> */}
        <h1>Orders</h1>
        {/* </a> */}
        <Table hover className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">is paid</th>
              <th scope="col">Location</th>
              <th scope="col">Total Price</th>
              <th style={{ width: "80px" }}>Delete</th>
              <th style={{ width: "80px" }}>Edit</th>
              <th style={{ width: "80px" }}>View</th>
            </tr>
          </thead>
          {orders && orders.map((val) => (
            <tbody>
              <tr>
                <th scope="row">{val.id}</th>

                <td>{val && val.user && val.user.name}</td>
                <td>{val.is_paid}</td>
                <td>{val && val.user && val.user.user_info && val.user.user_info.address1}</td>
                <td>{val.total_price}</td>
                <td>
                  <Button
                    size="sm"
                    variant="danger"
                    onClick={() => dispatch(removeOrder(val.id))}
                  >
                    Delete
                  </Button>
                </td>
                <td>
                  <Button
                    size="sm"
                    variant="info"
                    style={{ marginRight: "10px" }}
                    onClick={() => navigate(`/edit-order/${val.id}`)}
                  >
                    Edit
                  </Button>
                </td>
                <td>
                  <Button
                    size="sm"
                    variant="info"
                    style={{ marginRight: "10px" }}
                    onClick={() => navigate(`/view-order/${val.id}`)}
                  >
                    View
                  </Button>
                </td>
              </tr>
            </tbody>
          ))}
        </Table>
      </div>
    </div>
  );
}
