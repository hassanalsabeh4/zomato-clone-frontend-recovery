import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from 'react-router';
import { addCategory, updateCategory } from '../../redux/actions/categoryActions';
import Dashboard from '../dashboard/dashboard';

const EditCategory = () => {
  const { id } = useParams();
  const categories = useSelector((state) => state.categories.categories);
  const [editCategory, setEditCategory] = useState({})
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [editData, setEditData] = useState({})

  const onChange = (e) => {
    const { name, value } = e.target;

    setEditData((prev => {
      return {
        ...prev,
        [name]: value
      }
    }))

  }

  const submit = (e) => {
    e.preventDefault();
    dispatch(updateCategory(editData, id, navigate));
  }

  const add = (e) => {
    e.preventDefault();

    dispatch(addCategory(editData, navigate));
  }

  useEffect(() => {
    if (id) {

      setEditCategory(categories.filter((item) => item.id == id)[0]);
    }
    return () => {
      setEditCategory({});
    }
  }, [categories])

  return (
    <>
      <Dashboard />
      <div className="d-flex flex-lg-column  justify-content-center align-items-center vh-100">
        <h1 className="mb-3">{id !== "null" ? "Update" : "Add"}</h1>
        <div className="w-50">
          <form className="" onSubmit={id !== "null" ? submit : add}>
            <div class="form-group">
              <input
                type="text"
                id="name"
                class="form-control p-3"
                name="name"
                placeholder="Name"
                defaultValue={id && editCategory && editCategory.name}
                onChange={onChange}
              />
            </div>
            <input type="submit" class="btn btn-primary m-0 mt-3" value={id !== "null" ? "Update" : "Add"} />
          </form>
        </div>
      </div>
    </>

  );
}

export default EditCategory;