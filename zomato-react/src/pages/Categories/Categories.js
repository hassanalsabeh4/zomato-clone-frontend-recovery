import React, { useEffect } from "react";
import Dashboard from "../dashboard/dashboard";
import { Table } from "react-bootstrap";
import { useNavigate } from "react-router";
import Button from "react-bootstrap/Button";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
  deleteCategory,
  fetchCategory,
  getcategories,
} from "../../redux/actions/categoryActions";

export default function Categories() {
  const categories = useSelector((state) => state.categories.categories);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getcategories());
  }, []);

  return (
    <div>
      <Dashboard />

      <div style={{ margin: "5% 5% 0 20%" }}>
        {/* <a href=""> */}
        <Button
          variant="success"
          size="lg"
          block="block"
          type="submit"
          style={{ margin: "2% 0 2% 0" }}
          onClick={() => navigate(`/edit-category/${null}`)}
        >
          Add Categories
        </Button>
        {/* </a> */}
        <Table hover className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th style={{ width: "80px" }}>Delete</th>
              <th style={{ width: "80px" }}>Edit</th>
           
            </tr>
          </thead>
          <tbody>
            {categories &&
              categories.map((category) => (
                <tr>
                  <th scope="row">{category.id}</th>
                  <td>{category.name}</td>
                  <td>
                    <Button
                      size="sm"
                      variant="danger"
                      onClick={() =>
                        dispatch(deleteCategory(category.id, navigate))
                      }
                    >
                      Delete
                    </Button>
                  </td>
                  <td>
                    <Button
                      size="sm"
                      variant="info"
                      style={{ marginRight: "10px" }}
                      onClick={() => navigate(`/edit-category/${category.id}`)}
                    >
                      Edit
                    </Button>
                  </td>
                  
                </tr>
              ))}
          </tbody>
        </Table>
      </div>
    </div>
  );
}
